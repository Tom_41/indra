package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCMySQLConnection {
	// static reference to itself
	private static JDBCMySQLConnection instance = new JDBCMySQLConnection();

	// TODO - replace with own variables
	public static final String URL = "jdbc:mysql://localhost:3306/indra";
	public static final String USER = "root";
	public static final String PASSWORD = "root";
	public static final String DRIVER_CLASS = "com.mysql.jdbc.Driver";

	// private constructor
	private JDBCMySQLConnection() {
		// Loading class `com.mysql.jdbc.Driver'. -> This is deprecated.
	}

	private Connection createConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (SQLException e) {
			System.err.println("ERROR: Unable to Connect to Database.");
		}
		return connection;
	}

	public static Connection getConnection() {
		return instance.createConnection();
	}
}