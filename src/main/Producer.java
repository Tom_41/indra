package main;

import java.util.concurrent.BlockingQueue;

import user.Command;
import user.User;

public class Producer {

	public static Runnable serveCommands(BlockingQueue<Command> queue) {
		Runnable producer = () -> {
			try {
				Thread.sleep(Utils.getInterval(0, 1));
				queue.put(new Command(Command.TYPE.ADD, new User(1, "a1", "Robert")));
				System.out.printf("Producer: pushed new ADD command for User 1 into queue:%s \n", queue);
				
				Thread.sleep(Utils.getInterval(0, 1));
				queue.put(new Command(Command.TYPE.ADD, new User(2, "a2", "Martin")));
				System.out.printf("Producer: pushed new ADD command for User 2 into queue:%s \n", queue);
				
				Thread.sleep(Utils.getInterval(0, 1));
				queue.put(new Command(Command.TYPE.PRINT_ALL, null));
				System.out.printf("Producer: pushed PRINT_ALL command into queue:%s \n", queue);
				
				Thread.sleep(Utils.getInterval(0, 1));
				queue.put(new Command(Command.TYPE.DELETE_ALL, null));
				System.out.printf("Producer: pushed DELTET_ALL command into queue:%s \n", queue);
				
				Thread.sleep(Utils.getInterval(0, 1));
				queue.put(new Command(Command.TYPE.PRINT_ALL, null));
				System.out.printf("Producer: pushed PRINT_ALL command into queue:%s \n", queue);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		};
		return producer;
	}
}
