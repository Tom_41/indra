package main;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import user.Command;

public class Main {

	public static void main(String[] args) {
		BlockingQueue<Command> queue = new ArrayBlockingQueue<>(5);

		Runnable producerThread = Producer.serveCommands(queue);
		Runnable consumerThread = Consumer.consumeCommands(queue);

		new Thread(producerThread).start();
		new Thread(consumerThread).start();
	}
}
