package main;

import java.util.concurrent.ThreadLocalRandom;

public class Utils {
	static int[] intervals = { 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000 };

	// Randomly returns one of the value from intervals[] based on provided min and
	// max indexes
	public static int getInterval(int min, int max) {
		ThreadLocalRandom r = ThreadLocalRandom.current();
		int index = r.nextInt((max - min) + 1) + min;
		return intervals[index];
	}
}
