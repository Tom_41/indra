package main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import db.JDBCMySQLConnection;
import user.Command;
import user.User;

public class Consumer {

	public static Runnable consumeCommands(BlockingQueue<Command> queue) {
		Runnable consumer = () -> {
			while (true) {
				try {
					Thread.sleep(Utils.getInterval(2, 3));
					Command c = queue.take();
					if (c.getTpye() == Command.TYPE.ADD) {
						System.out.printf("Consumer: Adding user '%s' to DB.\n", c.getUser().toString());
						insert(c.getUser());
					} else if (c.getTpye() == Command.TYPE.PRINT_ALL) {
						System.out.printf("Consumer: Printing the users from DB:\n");
						getUsers().forEach(System.out::println);
					} else {
						System.out.printf("Consumer: Deleting the users from DB.\n");
						deletetAll();
					}
					if (queue.isEmpty()) {
						return;
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		};
		return consumer;
	}

	public static void insert(User u) {
		String query = "INSERT INTO susers (USER_ID,USER_GUID,USER_NAME) VALUES(?,?,?)";
		try (Connection conn = JDBCMySQLConnection.getConnection()) {
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setInt(1, u.getId());
			preparedStmt.setString(2, u.getGuid());
			preparedStmt.setString(3, u.getName());
			preparedStmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<User> getUsers() {
		String query = "SELECT * FROM susers";
		try (Connection conn = JDBCMySQLConnection.getConnection()) {
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			List<User> users = new ArrayList<>();
			ResultSet rs = preparedStmt.executeQuery();
			while (rs.next()) {
				users.add(new User(rs.getInt("USER_ID"), rs.getString("USER_GUID"), rs.getString("USER_NAME")));
			}
			return users;
		} catch (SQLException e) {
			e.printStackTrace();
			return Collections.emptyList();
		}
	}

	public static void deletetAll() {
		String query = "DELETE FROM susers;";
		try (Connection conn = JDBCMySQLConnection.getConnection()) {
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
