package test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.junit.jupiter.api.Test;

import main.Consumer;
import user.Command;
import user.User;

class ConsumerTest {

	@Test
	void testConsumeCommands() {
		BlockingQueue<Command> queue = new ArrayBlockingQueue<>(5);
		try {
			queue.put(new Command(Command.TYPE.ADD, new User(4, "t4", "Test")));
			queue.put(new Command(Command.TYPE.DELETE_ALL, null));
		} catch (InterruptedException e) {
			fail(e);
		}
		Runnable consumerThread = Consumer.consumeCommands(queue);
		Thread t = new Thread(consumerThread);
		t.start();
		try {
			t.join();
		} catch (InterruptedException e) {
			fail(e);
		}
		assertEquals(0, Consumer.getUsers().size());
	}

	@Test
	void testInsert() {
		int sizeBefore = Consumer.getUsers().size();
		Consumer.insert(new User(3, "t3", "Test"));
		assertEquals(sizeBefore + 1, Consumer.getUsers().size());
		Consumer.deletetAll();
	}

	@Test
	void testGetUsers() {
		assertDoesNotThrow(() -> Consumer.getUsers());
	}

	@Test
	void testDeletetAll() {
		Consumer.deletetAll();
		assertEquals(0, Consumer.getUsers().size());
	}

}
