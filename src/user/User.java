package user;

public class User {

	private int id;
	private String guid;
	private String name;

	public User(int id, String guid, String name) {
		this.id = id;
		this.guid = guid;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", guid=" + guid + ", name=" + name + "]";
	}
}