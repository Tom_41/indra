package user;

public class Command {

	public static enum TYPE {
		ADD, DELETE_ALL, PRINT_ALL
	}

	private TYPE tpye;
	private User user;

	public Command(TYPE tpye, User user) {
		this.tpye = tpye;
		this.user = user;
	}

	public TYPE getTpye() {
		return tpye;
	}

	public void setTpye(TYPE tpye) {
		this.tpye = tpye;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Command [type=" + tpye + "]";
	}
}